#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main()
{
    char **carr;
    int num;
    int i;
    int row_num;
    char str[70];

    printf("enter number:"); 
    scanf("%d", &num);

    // Do not typecast malloc as it may hide potential bug by not including stdlib.h
    //carr = (char**) malloc(sizeof(char*) * num); 
    carr = malloc(sizeof(char*) * num);

    for(i=0;i<num;i++)
    {
        scanf("%s", str);
        row_num = strlen(str);
        printf("strlen(str) = %d\n", row_num);
        carr[i] = malloc(sizeof(char) * row_num);
        strcpy(carr[i], str);
    } 
    for(i=0;i<num;i++)
    {
        row_num = strlen(carr[i]);
        printf("%s %d\n", carr[i], row_num);
    } 
}
