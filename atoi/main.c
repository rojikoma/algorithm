#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int _atoi(char cSrc[]);
//nval 변환하고자 하는 값, cres = 변환된 문자열을 저장할 참조변수, nBase = 변환하고자 하는 진법
void _itoa(int nVal, char cRes[], int nBase);


int _atoi(char cSrc[])
{
    int i,j;
    int str_len;
    int digit;
    int newNum;
    int sscript;
    
    str_len = strlen(cSrc);
    printf("strlen = %d\n", str_len);
    for(i=0;i<str_len;i++)
    {
        digit = (int)(cSrc[i] - 48);
        printf("digit = %d\n", digit);
        sscript = 1;

        for(j=1;j<(str_len - i);j++)
            sscript *= 10;  
        newNum = newNum + (digit * sscript);
    }
    printf("newNum = %d\n", newNum);
}

void _itoa(int nVal, char cRes[], int nBase)
{
    char temp[64];
    char tempDigit;
    int digit;
    int i = 0;
    int j = 0;
    int pos = 0;

    while(nVal != 0)
    {
        digit = nVal % nBase;
        nVal = nVal / nBase;
        printf("nval = %d , digit = %d", nVal, digit);
 
        if (nBase == 16)
        {
            if (digit >= 10 && digit <= 15)
                tempDigit = (char)(digit + 87);
            else
                tempDigit = (char)digit + '0';
        }
        else
            tempDigit = (char)digit + '0';

        temp[i] = tempDigit;
        printf("      temp[%d]=%c\n ", i, temp[i]); 
        i++;
    }

    for(j=0;j<i;j++)
    {
        cRes[j] = temp[i-1-j];
        printf("cRes[%d] = %c ", j, cRes[j]); 
    }

}


int main()
{
    int num;
    char buf[64];
    int base;
    int atoi_val;

    printf("enter num");
    scanf("%s", buf);
    atoi_val = _atoi(buf);
    
    printf("enter num and conversion");
    scanf("%d %d", &num, &base);
    _itoa(num, buf, base);
    printf("\nconverted number in stirng is %s\n", buf); 
}
